//global variables
var pokemonList = [];         //array of pokemon objects
var pokemonNames = [];        //array of pokemon names
var currentPokemon = [];      //the pokemon object that is searched
var pokemonExists = false;    //whether or not a valid pokemon is showing on screen
var typeSpecials = {"bug":0,"dark":0,"dragon":0,"electric":0,"fairy":0,"fighting":0,"fire":0,"flying":0,"ghost":0,
"grass":0,"ground":0,"ice":0,"normal":0,"poison":0,"psychic":0,"rock":0,"steel":0,"water":0};        //keeps track of types and which ones it resists/is vulnerable to

//load data into pokemonList and pokemonNames
$(document).ready(function() {
    console.log("got here");
    $.ajax({
        type: "GET",
        url: "../data/pvp_info_2.csv",
        dataType: "text",
        success: function(data) {processData(data);}
     });
});

function processData(allText) {
    var allTextLines = allText.split(/\r\n|\n/);
    var headers = allTextLines[0].split(',');

    for (var i=1; i<allTextLines.length; i++) {

        var data = allTextLines[i].split(',');

        if (data.length == headers.length) {

            var pokemonObject = {"name":""};
            for (var j=0; j<headers.length; j++) {
                pokemonObject[headers[j]] = data[j];
            }

            var keyName = ""+pokemonObject.name.toLowerCase();
            window.pokemonList[keyName]=pokemonObject;
            window.pokemonNames.push(keyName);
        }
    }
    window.pokemonNames.sort();
}

//typing into the text input
document.addEventListener('DOMContentLoaded', function() {
    var inputValue = document.getElementById('pokemonList');
    inputValue.addEventListener('input', function() {
        showOptions();
    });
});

function showOptions() {
  var inputValue = document.getElementById("pokemonList").value.toLowerCase();
  var optionsList = "";

  window.pokemonNames.forEach((function (name, index, array) {
    var option = name.toLowerCase();
    var isAlolan = option.includes("alolan")
    if (isAlolan){
      console.log(option);
    }

    if (inputValue.length <= option.length) {
      if (inputValue==option.slice(0,inputValue.length)){
        //console.log(option);
        var clean_option = option.slice(0,1).toUpperCase()+option.slice(1);
        console.log(clean_option);
        //optionsList = optionsList + '<option value="' + +clean_option + '">';
        optionsList = optionsList+ `<option value="${clean_option}">`;
      }
    }
  }));

  document.getElementById("pokemonListOptions").innerHTML = optionsList;

}

//what happens after search button is pressed:
//1) check if pokemon name is valid i.e is it in pokemonNames
//2) if pokemon name valid, set value of pokemonList to be the pokemon name
//3) get pokemon object to show its stats


$(document).ready(function () {
  // Listen to submit event on the <form> itself!
  $('#searchForm').submit(function (e) {

    e.preventDefault();
    checkPokemonExistence();
    updateCurrentPokemon();
    updateDisplay();
    //console.log('searching');

  });
});


function checkPokemonExistence() {
  var inputValue = document.getElementById("pokemonList").value.toLowerCase();

  var match = false;

  window.pokemonNames.forEach((function (name, index, array) {
    var pName = name.toLowerCase();
    if (inputValue == pName) {
      match = true;
    }
  }));

  if (match){
    window.pokemonExists = true;
  }
  else{
    window.pokemonExists = false;
  }

  if (window.pokemonExists) {
    window.currentPokemon = window.pokemonList[inputValue];
  }
  else{
    window.currentPokemon = [];
  }

}

function updateCurrentPokemon(){
  if (window.pokemonExists){
    //document.getElementById("searchBlock").innerHTML = window.currentPokemon.name;
    console.log(JSON.stringify(window.currentPokemon));
  }
  else{
    //document.getElementById("searchBlock").innerHTML = "Cannot find Pokemon. Please try again.";
    alert("Cannot find Pokemon. Please try again.");
  }
}

function updateDisplay(){
  if (window.pokemonExists){

    //RANKS
    //great league rank
    if (window.currentPokemon.greatRank!=-1 & window.currentPokemon.greatRank!="N"){
      document.getElementById("greatLeagueRank").innerHTML = window.currentPokemon.greatRank;
    }
    else{
      document.getElementById("greatLeagueRank").innerHTML = "Unranked";
    }

    //ultra league rank
    if (window.currentPokemon.ultraRank!=-1 & window.currentPokemon.ultraRank!="N"){
      document.getElementById("ultraLeagueRank").innerHTML = window.currentPokemon.ultraRank;
    }
    else{
      document.getElementById("ultraLeagueRank").innerHTML = "Unranked";
    }

    //master league rank
    if (window.currentPokemon.masterRank!=-1 & window.currentPokemon.masterRank!="N"){
      document.getElementById("masterLeagueRank").innerHTML = window.currentPokemon.masterRank;
    }
    else{
      document.getElementById("masterLeagueRank").innerHTML = "Unranked";
    }

    //IMAGE OF POKEMON
    //var imageFile = "../data/images/"+window.currentPokemon.name.toLowerCase()+".png";

    var backupImageFile = "https://img.pokemondb.net/sprites/go/normal/";
    var backupBackupImageFile = "https://img.pokemondb.net/sprites/go/normal/";
    var backupBackupBackupImageFile = "https://img.pokemondb.net/sprites/home/normal/";
    var cleanImageNameHeader = "https://img.pokemondb.net/sprites/go/normal/";

    var cleanImageName = window.currentPokemon.name.toLowerCase().trim().replace(/['-]/g,'');
    if (cleanImageName.includes("shadow")){
        cleanImageName = cleanImageName.replace("shadow ","");
    }
    if (cleanImageName=="hooh"){
      cleanImageName="ho-oh";
    }
    if (cleanImageName=="porygonz"){
      cleanImageName="porygon-z";
    }
    if (cleanImageName.includes("darmanitan")){
      var newCleanImageName = "darmanitan-"
      if (cleanImageName.includes("galarian")){
        newCleanImageName+="galarian-";
        cleanImageNameHeader = "https://img.pokemondb.net/sprites/home/normal/";
        backupImageFile = "https://img.pokemondb.net/sprites/home/normal/";
        backupBackupImageFile = "https://img.pokemondb.net/sprites/home/normal/";
        if (cleanImageName.includes("zen")){
          newCleanImageName+="zen";
        }
        else{
          newCleanImageName+="standard";
        }
      }
      else{
        if (cleanImageName.includes("zen")){
          newCleanImageName+="zen";
        }
        else{
          newCleanImageName+="standard";
        }
      }
      cleanImageName = newCleanImageName;
    }

    else if (cleanImageName.includes(" ")){
      if (cleanImageName.includes("alolan")){
        cleanImageName = cleanImageName.split(" ")[1]+"-alolan";
      }
      else if (cleanImageName.includes("galarian")){
        cleanImageNameHeader = "https://img.pokemondb.net/sprites/home/normal/";
        var cleanImageNameParts = cleanImageName.split(" ");
        cleanImageName = cleanImageNameParts[1]+"-galarian";
        if (cleanImageNameParts.length>2){
          cleanImageName = cleanImageNameParts[1]+"-galarian-"+cleanImageNameParts[2];
        }
      }
      else{
        var cleanImageNameParts = cleanImageName.split(" ");
        cleanImageNameParts.forEach((function (part, index, array) {
          var newString = part.replace(/[()']/g,'');
          cleanImageNameParts[index] = newString;
        }));
        cleanImageName = cleanImageNameParts[0]+"-"+cleanImageNameParts[1];
        backupImageFile = backupImageFile+cleanImageNameParts[1]+"-"+cleanImageNameParts[0]+".png";
        backupBackupImageFile = backupBackupImageFile+cleanImageNameParts[1]+"-"+cleanImageNameParts[3]+".png";
      }
    }

    else{
      var a =2;
    }

    var imageFile = cleanImageNameHeader + cleanImageName + ".png";
    backupBackupBackupImageFile =  backupBackupBackupImageFile + cleanImageName + ".png";

    var pImage = document.getElementById("pokemonImage");

    pImage.setAttribute("src", imageFile);

    pImage.onerror = function() {
      document.getElementById("pokemonImage").setAttribute("src", backupImageFile);
      console.log(backupImageFile);
      pImage.onerror = function() {
        document.getElementById("pokemonImage").setAttribute("src", backupBackupImageFile);
        console.log(backupBackupImageFile);
        pImage.onerror = function() {
          document.getElementById("pokemonImage").setAttribute("src",backupBackupBackupImageFile);
          console.log(backupBackupBackupImageFile);
          pImage.onerror = function(){
            alert("Error loading " + this.src); // Error loading https://example.com/404.js
            imageFile = "../data/images/default.png";
            document.getElementById("pokemonImage").setAttribute("src", imageFile);
          }

        };
      };
    };

    //BEST MOVESET
    var bestMoves = window.currentPokemon.bestMoveCombos.trim();
    if (bestMoves.length>0){
      document.getElementById("bestMoveSet").innerHTML = "Best moves: "+bestMoves;
    }
    else{
      document.getElementById("bestMoveSet").innerHTML = "...";
    }

    //TYPE of POKEMON
    var type1 = window.currentPokemon.type1;
    var type2 = window.currentPokemon.type2;
    var imageFile = "../data/images/type_highlight_4.png";

    var typeTags = []; //keeps track of tags for indicating the type

    var posCounter = 0;
    for (let key in window.typeSpecials){
      if (key==type1 | key==type2){
        var typeTag = "type_deg"+(posCounter*20);
        typeTags.push(typeTag);
      }
      posCounter ++;
    }

    for (var i=0; i<18; i++){
      var typeTag = "type_deg"+(i*20);
      if (typeTags.includes(typeTag)){
        document.getElementById(typeTag).setAttribute("src",imageFile);
      }
      else{
        document.getElementById(typeTag).removeAttribute("src");
      }
    }


    //TYPES IT RESISTS AND IS VULNERABLE TO
    var resistList = window.currentPokemon.typeAdvantages.split("&");
    var vulnerableList = window.currentPokemon.typeDisadvantages.split("&");

    var posCounter = 0;
    for (let key in window.typeSpecials) {

      var imageKey = "deg";
      var noImageKey1 = "deg";
      var noImageKey2 = "deg";

      var imageFile = "../data/images/"+key+".png";
      if (window.typeSpecials.hasOwnProperty(key)) {
        if (resistList.includes(key)){
          window.typeSpecials[key] = 1;
          imageKey=imageKey+(20*posCounter);
          noImageKey1 = noImageKey1+(20*posCounter)+"_mid";
          noImageKey2 = noImageKey2+(20*posCounter)+"_inner";
        }
        else if (vulnerableList.includes(key)){
          window.typeSpecials[key] = -1;
          imageKey=imageKey+(20*posCounter)+"_inner";
          noImageKey1 = noImageKey1+(20*posCounter);
          noImageKey2 = noImageKey2+(20*posCounter)+"_mid";
        }
        else{
          window.typeSpecials[key] = 0;
          imageKey=imageKey+(20*posCounter)+"_mid";
          noImageKey1 = noImageKey1+(20*posCounter)+"_inner";
          noImageKey2 = noImageKey2+(20*posCounter);
        }

        document.getElementById(noImageKey1).removeAttribute("src");
        document.getElementById(noImageKey2).removeAttribute("src");

        var tImage = document.getElementById(imageKey)
        tImage.setAttribute("src",imageFile);

        tImage.onerror = function() {
          alert("Error loading type image" + this.src); // Error loading https://example.com/404.js
          imageFile = "../data/images/default.png";
          document.getElementById("pokemonImage").setAttribute("src", imageFile);
        };

      }
      posCounter++;
    }

    //LINK to gamepress
    document.getElementById("moreInfo").innerHTML = "Click here for more information!";
    document.getElementById("moreInfo").setAttribute("href",window.currentPokemon.url);


  }
}
