//colours from pastel palette: https://www.anthologyprint.com/color-palettes

clrs = ["#FFB3AB","#FFB1BB","#FC9BB3","#FB637E","#F4364C",
        "#EABEDB","#F1A7DC","#F4A6D7","#F277C6","#E93CAC",
        "#E7BAE4","#D7A9E3","#C1A0DA","#A77BCA","#8246AF",
        "#ABCAE9","#92C1E9","#69B3E7","#6CACE4","#0072CE",
        "#A7E6D7","#8CE2D0","#6ECEB2","#00B388","#009775",
        "#D4EB8E","#CDEA80","#DBE442","#CEDC00","#C4D600",
        "#F8E08E","#FBD872","#FED141","#FFC72C","#FFB81C",
        "#FFC27B","#FFAE62","#FFB25B","#FF7F32","#FF6A13"]

        //grey->black values
        //["#D0D3D4","#A2AAAD","#7C878E","#333F48","#1D252D"]

index = Math.floor(Math.random() * clrs.length)
document.body.style.backgroundColor = clrs[index];
var circleMid = document.getElementsByClassName("circle-container_mid")[0];
console.log(circleMid);
circleMid.style.backgroundColor = clrs[index];
